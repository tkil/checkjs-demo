export interface IObject {
  [ key: string]: any
}

export interface IStudent {
  id: number
  firstName: string
  lastName: string
  period: number
  isPresent: boolean
}
