import { IStudent } from "../../../types";
import { html, render, TemplateResult } from "lit-html";
// Project
import { safeParseJson } from "../../utils/misc";

const componentName = "student-roster";

const style = /*css*/ `
  .student-roster {
    margin: 0 auto;
  }
  .student-roster__table {
    margin: 0 auto;
  }
`;

/**
 * Renders the table header for student roster
 * @return {TemplateResult}
 */
const renderHeader = () =>
  html`
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Period</th>
        <th>Present</th>
      </tr>
    </thead>
  `;

/**
 * This callback type is called `requestCallback` and is displayed as a global symbol.
 *
 * @callback OnStudentPresentClickCallback
 * @param {Event} event - click event
 * @return {void}
 */
/**
 * Renders the body of the student roster table
 * @param {IStudent[]} students - Array of students in the roster
 * @param {OnStudentPresentClickCallback} onStudentPresentClick - Checks and unchecks student attendance
 * @return {TemplateResult}
 */
const renderBody = (students, onStudentPresentClick) =>
  html`
    <tbody>
      ${students.map((student) => {
        return html`
          <tr>
            <td>${student.firstName}</td>
            <td>${student.lastName}</td>
            <td>${student.period}</td>
            <td>
              <input
                type="checkbox"
                id="is-present"
                name="is-present"
                data-student-id=${student.id}
                .checked=${student.isPresent}
                .onclick="${onStudentPresentClick}"
              />
            </td>
          </tr>
        `;
      })}
    </tbody>
  `;

/**
 * Named args for renderFooter
 * @typedef {Object} RenderFooterArgs
 * @property {number} totalPresent - Number of students present
 * @property {number} totalAll - All students
 */
/**
 * Renders the footer for the student roster table
 * @param {RenderFooterArgs} args - named args
 * @return {TemplateResult}
 */
const renderFooter = ({ totalPresent, totalAll }) =>
  html`
    <tfoot>
      <tr>
        <th></th>
        <th></th>
        <th>Total</th>
        <th>${totalPresent} / ${totalAll}</th>
      </tr>
    </tfoot>
  `;

/**
 * Template function for the student roster table
 * @param {Object[]} students - Array of students in the roster
 * @param {OnStudentPresentClickCallback} onStudentPresentClick - Checks and unchecks student attendance
 * @return {TemplateResult}
 */
const template = (students, onStudentPresentClick) => {
  const totalPresent = students.filter((s) => s.isPresent).length;
  const totalAll = students.length;
  return html`
    <style>
      ${style}
    </style>
    <table class="student-roster__table table">
      ${renderHeader()} ${renderBody(students, onStudentPresentClick)} ${renderFooter({ totalPresent, totalAll })}
    </table>
  `;
};

window.customElements.define(
  componentName,
  class extends HTMLElement {
    constructor() {
      super();
      this._students = this._attributeToStudents(this.getAttribute("students"));
    }

    /**
     * Connected callback for student-roster
     * @return {void}
     */
    connectedCallback() {
      render(template(this._students, this._handleAttendanceUpdate.bind(this)), this);
    }

    /**
     * Private event helper for custom attendanceUpdate event
     * @param {MouseEvent} event - Click event for standard html input checkbox element
     * @private
     */
    _handleAttendanceUpdate(event) {
      const checkElement = /** @type {HTMLInputElement} */ (event.target);
      const studentId = Number.parseInt(checkElement.getAttribute("data-student-id"), 10);
      const isPresent = checkElement.checked;
      this.dispatchEvent(
        new CustomEvent("onAttendanceUpdate", {
          detail: { studentId, isPresent },
        })
      );
    }

    /**
     * Converts string HTML attribute to array of students
     * @param {string} attribute - element attribute
     * @return {IStudent[]} students
     */
    _attributeToStudents(attribute) {
      return /** @type {IStudent[]} */ (/** @type {any} */ (safeParseJson(attribute) || []));
    }

    /**
     * Observed attributes for student-roster
     * @return {string[]} - List of attribute changes we care about
     */
    static get observedAttributes() {
      return ["students"];
    }

    /**
     * Attribute changed callback for student-roster
     * @param {string} attrName - name of changed attribute
     * @param {string} _ - old value; not currently used
     * @param {string} newVal - new value
     * @return {void}
     */
    attributeChangedCallback(attrName, _, newVal) {
      switch (attrName) {
        case "students":
          this._students = this._attributeToStudents(newVal);
          render(template(this._students, this._handleAttendanceUpdate.bind(this)), this);
          return;
      }
    }
  }
);
