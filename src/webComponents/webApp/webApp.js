import { IStudent } from "../../../types";
import { html, render, TemplateResult } from "lit-html";
// Project
import mockData from "../../data/mockStudents.js";

const componentName = "web-app";

const style = /*css*/ `
  .web-app {
    padding: 24px;
  }
`;

/**
 * Template function for the web-app component
 * @param {IStudent[]} students - array of students
 * @return {TemplateResult}
 */
const template = (students) => {
  const strStudents = JSON.stringify(students);
  return html`
    <style>
      ${style}
    </style>
    <div class="web-app">
      <student-roster students=${strStudents}></student-roster>
    </div>
  `;
};

window.customElements.define(
  componentName,
  class extends HTMLElement {
    constructor() {
      super();
      this._students = mockData;
    }

    /**
     * Connected callback for web-app
     * @return {void}
     */
    connectedCallback() {
      render(template(this._students), this);
      const studentRosterElement = document.querySelector("student-roster");
      if (studentRosterElement) {
        studentRosterElement.addEventListener("onAttendanceUpdate", this._handleStudentAttendanceUpdate.bind(this));
      }
    }

    /**
     * Private event helper for handling custom onAttendanceUpdate event
     * @param {CustomEvent} event - Custom onAttendanceUpdate event for student-roster
     * @private
     */
    _handleStudentAttendanceUpdate(event) {
      const { studentId, isPresent } = event.detail;
      this._students.find((student) => student.id === studentId).isPresent = isPresent;
      this._updateStudents();
    }

    /**
     * Update re-renders web-app
     * @return {void}
     * @private
     */
    _updateStudents() {
      const studentRosterElement = document.querySelector("student-roster");
      if (studentRosterElement) {
        const strStudents = JSON.stringify(this._students);
        studentRosterElement.setAttribute("students", strStudents);
      }
    }
  }
);
