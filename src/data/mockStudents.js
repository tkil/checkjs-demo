export default [
  {
    id: 123,
    firstName: "Billy",
    lastName: "Green",
    period: 1,
    isPresent: true,
  },
  {
    id: 421,
    firstName: "Todd",
    lastName: "Smith",
    period: 1,
    isPresent: false,
  },
  {
    id: 132,
    firstName: "Max",
    lastName: "Anderson",
    period: 1,
    isPresent: true,
  },
  {
    id: 223,
    firstName: "Ryan",
    lastName: "Dugan",
    period: 3,
    isPresent: true,
  },
  {
    id: 432,
    firstName: "Duke",
    lastName: "Whippet",
    period: 3,
    isPresent: false,
  },
];
