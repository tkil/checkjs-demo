import { IObject } from "../../types";

/**
 * Attempts a JSON parse, but returns undefined on failure and swallows exception
 * @param {string} strJson - Stringified json to parse
 * @return {IObject | IObject[]} The result object of the json parse
 */
export const safeParseJson = (strJson) => {
  {
    try {
      return JSON.parse(strJson);
    } catch (error) {
      return {};
    }
  }
};

/**
 * An a generic object type local to misc.js
 * @typedef {Object.<string, string>} IObject2
 */
/**
 * Attempts a JSON parse, but returns undefined on failure and swallows exception
 * @param {string} strJson - Stringified json to parse
 * @return {IObject2 | IObject2[]} The result object of the json parse
 */
export const safeParseJsonExample2 = (strJson) => {
  {
    try {
      return JSON.parse(strJson);
    } catch (error) {
      return {};
    }
  }
};

export const addNoTypes = (numberA, numberB) => {
  return numberA + numberB;
};

/**
 * Adds two numbers
 * @param {number} numberA - first number
 * @param {number} numberB - second number
 * @return {number} the result
 */
export const addWithTypes = (numberA, numberB) => {
  return numberA + numberB;
};

/**
 * Tags a name with a number
 * @param {string} name - some name
 * @param {number} number - some number
 * @return {string} the string result
 */
export const nameNumTag = (name, number) => {
  return `${name} - ${number}`;
};
